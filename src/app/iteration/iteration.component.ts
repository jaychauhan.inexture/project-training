import { Component, OnInit } from '@angular/core';
import { Hero } from '../hero';
import { HeroService } from '../hero.service';

@Component({
  selector: 'app-iteration',
  templateUrl: './iteration.component.html',
  styleUrls: ['./iteration.component.css']
})
export class IterationComponent implements OnInit {

  heroes: Hero[] = [];
  customers = [{value: 'Ebony'}, {value: 'Chiho'}];
  customer = 'Padma';

  constructor(private heroService: HeroService) { }

  ngOnInit(): void {
    this.getHeroes();
  }

  getHeroes() {
    this.heroService.getHeroes().subscribe(heroes => this.heroes = heroes.slice(1, 5));
  }

  trackHero(index: number, hero: Hero) {
    console.log(hero);
    return hero ? hero.id : undefined;
  }

}
