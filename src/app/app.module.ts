import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SampleComponentComponent } from './sample-component/sample-component.component';
import { FormsModule } from '@angular/forms';
import { UserComponent } from './user/user.component';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { MessagesComponent } from './messages/messages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { IterationComponent } from './iteration/iteration.component';
import { AdDirective } from './ad.directive';
import { AdBannerComponent } from './ad-banner/ad-banner.component';
import { AdService } from './ad.service';
import { DynamicLoaderComponent } from './dynamic-loader/dynamic-loader.component';
import { HeroProfileComponent } from './hero-profile/hero-profile.component';
import { HeroJobAdComponent } from './hero-job-ad/hero-job-ad.component';
import { DemopipePipe } from './demopipe.pipe';

@NgModule({
  declarations: [
    AppComponent,
    SampleComponentComponent,
    UserComponent,
    UserDetailComponent,
    MessagesComponent,
    DashboardComponent,
    IterationComponent,
    AdDirective,
    AdBannerComponent,
    DynamicLoaderComponent,
    HeroProfileComponent,
    HeroJobAdComponent,
    DemopipePipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [AdService],
  entryComponents: [HeroProfileComponent, HeroJobAdComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
