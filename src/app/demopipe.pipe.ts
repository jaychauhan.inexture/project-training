import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'demopipe'
})
export class DemopipePipe implements PipeTransform {

  transform(value: number, extension: string = ' Kilograms'): unknown {
    return (value * 1000).toFixed(2) + extension;
  }

}
