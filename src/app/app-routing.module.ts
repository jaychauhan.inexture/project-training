import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdBannerComponent } from './ad-banner/ad-banner.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DynamicLoaderComponent } from './dynamic-loader/dynamic-loader.component';
import { IterationComponent } from './iteration/iteration.component';
import { SampleComponentComponent } from './sample-component/sample-component.component';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { UserComponent } from './user/user.component';

const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'users', component: UserComponent },
  { path: 'detail/:id', component: UserDetailComponent },
  { path: 'sample', component: SampleComponentComponent },
  { path: 'iteration', component: IterationComponent },
  { path: 'dynamic/component/loader', component: DynamicLoaderComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
