import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sample-component',
  templateUrl: './sample-component.component.html',
  styleUrls: ['./sample-component.component.css']
})
export class SampleComponentComponent implements OnInit {
  constructor() { }

  ngOnInit(): void {
  }

  title: string = "sample template";
  userName: string = "";

  caption: string = "Click Me";
  public no: number = 0;
  randomNo = [2, 6, 8, 9, 54, 7, 82, 66, 27, 50];

  birthday = new Date(1988, 3, 15); // April 15, 1988 -- since month parameter is zero-based
  toggle = true; // start with true == shortDate

  get format() { return this.toggle ? 'shortDate' : 'fullDate'; }
  toggleFormat() { this.toggle = !this.toggle; }

  private count: number = 0;

  sample() {
    alert("hello " + this.userName);
  }

  callPhone(value: any) {
    alert(value);
  }

  getCurrentTime(): any {
    return Date.now();
  }

}
